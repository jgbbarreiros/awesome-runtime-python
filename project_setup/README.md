# Project Setup

The guide to get you started!

## initial_setup

For demonstrations purposes let's say we decided to create a project called `smul`! This will be a simple URL shortening service website. So what do we have to do to get started? Let's start by creating our python project folder, and cd into it.

```shell
mkdir smul
cd smul
```

Let's choose the python version we want to work with for our project. This will create a file in our current directory called `.pyenv` containing the version we just specified.

```shell
pyenv local 3.10.0
```

Now let's create a virtual environment for all our python project dependencies. This command might not seem obvious so let's dissect it.

We can use `python -m` to run a python module in this case the `venv` module that allows us to create virtual environments. Our virtual environment is going to be called `venv` since is the norm on python projects even though you can call it whatever you want. Now the last two are optional but I like to do them myself and I'm just going to leave you with their description. [PEP 405](https://www.python.org/dev/peps/pep-0405) More information on virtual environments.

```shell
$ python -m venv -h
  ...
  --prompt PROMPT       Provides an alternative prompt prefix for
                        this environment.
  --upgrade-deps        Upgrade core dependencies: pip setuptools
                        to the latest version in PyPI
```

```shell
python -m venv venv --upgrade-deps --prompt smul
```

Before we start installing our dependencies we need to activate our environment. When we created our virtual environment a folder called `venv` should've pop up on your project directory, inside that folder is our activation script amongst other things. Activate your virtual environment by running this.

```shell
source venv/bin/activate
```

As a confirmation you should now see something like this on your terminal.

```shell
(smul) $
```

✨ GOOD JOB ✨ we now have everything we need to start setting up our python project.

## packages

Not every project need to be a python package but ultimately they can, and even though python official documentation doesn't talk a lot about how you should setup a python project they talk a lot about [packages](https://packaging.python.org/tutorials/packaging-projects). Let's then learn about packages and apply the same knowledge to our projects.

### folder_structure

Here is simple project folder structure layout. The term repository is not a technical one and in this case is the same thing as project, [packages](https://docs.python.org/3/tutorial/modules.html#packages) and [modules](https://docs.python.org/3/tutorial/modules.html#modules) however have special meanings and they are structured like so:

```tree
repository/
├── package
│   ├── __init__.py
│   ├── __main__.py
│   └── module.py
├── README.md
└── setup.py
```

### naming_convention

[PEP 8](https://www.python.org/dev/peps/pep-0008) (Style Guide for Python Code) has this topic about [Package and Module Names](https://www.python.org/dev/peps/pep-0008/#package-and-module-names) that states the following:

>Modules should have short, all-lowercase names. Underscores can be used in the module name if it improves readability. Python packages should also have short, all-lowercase names, although the use of underscores is discouraged.

And [PEP 423](https://www.python.org/dev/peps/pep-0423) (Naming conventions and recipes related to packaging) also has this topic about [Use a single name](https://www.python.org/dev/peps/pep-0423/#use-a-single-name):

>Distribute only one package (or only one module) per project, and use package (or module) name as project name.

This means means that if our little project was called **Copy Pasta** our project structure would look something like this

```tree
copy-pasta/
├── copy_pasta
│   ├── __init__.py
│   ├── __main__.py
│   └── copy_pasta.py
├── README.md
└── setup.py
```

I would also like to point out that when is stated the *...use of underscores is discouraged.* is probably just to encourage you to keep the names short.

The use of underscores actually happens a lot especially when you're publishing a package that is an extension of a popular framework since you would prefix the package name with the name of the framework, for example [flask_admin](https://github.com/flask-admin/flask-admin).

## style_guides

The python language has this things called Python Enhancement Proposals more usually known as PEP. There are two PEPs that are the source of truth, nay, the holy grail of all python style guides.

- [PEP 257](https://www.python.org/dev/peps/pep-0257) (Docstring Conventions)
- [PEP 8](https://www.python.org/dev/peps/pep-0008) (Style Guide for Python Code)

### linters_and_formatters

From this extensive documents most linters and formatter try to adhere to these conventions. Ones are more strict than other but all of them try to make your code more beautiful, consistent and easier to ready.

| Linter                                    | Description                                                                                            |
| ----------------------------------------- | ------------------------------------------------------------------------------------------------------ |
| [pylint](https://pypi.org/project/pylint) | Checks for errors, tries to enforce a coding standard and looks for code smells.                       |
| [flake8](https://pypi.org/project/flake8) | Wrapper around pyflakes, pycodestyle and mccabe. Capable of detecting both logical and stylistic lint. |

| Formatter                               | Description                                                            |
| --------------------------------------- | ---------------------------------------------------------------------- |
| [black](https://pypi.org/project/black) | Black is a PEP 8 compliant opinionated formatter.                      |
| [yapf](https://pypi.org/project/yapf)   | Formatting that conforms to the style guide.                           |
| [isort](https://pypi.org/project/isort) | Formats imports by sorting alphabetically and separating into sections |

### setups

- [black everything](linters_and_formatters/black.md) - black + flake8/pylint + isort + pre-commit

### reference links

- vscode formatters - <https://code.visualstudio.com/docs/python/editing#_formatterspecific-settings>
- vscode linters - <https://code.visualstudio.com/docs/python/linting#_specific-linters>
- Real Python - Python Code Quality: Tools &amp; Best Practices - <https://realpython.com/python-code-quality/>
- Python Linters and Code Analysis tools curated list - <https://github.com/vintasoftware/python-linters-and-code-analysis>
- Keeping python code clean with pre-commit hooks: black, flake8 and isort - <https://medium.com/staqu-dev-logs/keeping-python-code-clean-with-pre-commit-hooks-black-flake8-and-isort-cac8b01e0ea1>
