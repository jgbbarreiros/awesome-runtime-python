# black + flake8/pylint + isort + pre-commit

Everything **black**.

## black

Visit the [project](https://pypi.org/project/black) on PyPI.

> Black is the uncompromising Python code formatter. By using it, you agree to cede control over minutiae of hand-formatting. In return, Black gives you speed, determinism, and freedom from pycodestyle nagging about formatting. You will save time and mental energy for more important matters.

```shell
pip install black
```

~~Configure `black`~~

`black` is not really configurable. This is basically the most you can do.

`pyproject.toml`

```toml
[tool.black]
line-length = 88
skip-string-normalization = true # optional (default=false)
```

### IMPORTANT configure editor

Setup `black` with your editor of choice [here](https://github.com/psf/black/blob/master/docs/editor_integration.md).

This is how you're going to make everything work. Your should probably configure your editor to format on save.

---

## isort

Visit the [project](https://pypi.org/project/isort) on PyPI.

> isort your imports, so you don't have to.

```shell
pip install isort
```

Configure `isort` according to `black`.

`pyproject.toml`

```toml
[tool.isort]
profile = "black"
```

---

## pylint

Visit the [project](https://pypi.org/project/pylint) on PyPI.

> It’s not just a linter that annoys you!

Using `pylint` ?

```shell
pip install pylint
```

Configure `pylint` according to `black`.

`pyproject.toml`

```toml
[tool.pylint.messages_control]
disable = "C0330, C0326"

[tool.pylint.format]
max-line-length = "88"
```

## flake8

Visit the [project](https://pypi.org/project/flake8) on PyPI.

> flake8 is a python tool that glues together pep8, pyflakes, mccabe, and third-party plugins to check the style and quality of some python code.

Using `flake8` ?

```shell
pip install flake8
```

Configure flake8 according to `black`.

Unfortunately flake8 doesn't allow you to configure it in ~~`pyproject.toml`~~ so for the time being use:

`.flake8` or `setup.cfg`

```toml
[flake8]
max-line-length = 88
extend-ignore = E203
```

---

## pre-commit

Visit the [project](https://pypi.org/project/pre-commit) on PyPI.

```shell
pip install pre-commit
```

Now let's setup `pre-commit` to ensure a *Fail Fast* methodology. These pre-commit-hooks will allow us to ensure that our code don't get committed without being properly formatted.

Checkout you're installed `black` and `isort` package versions.

```shell
$ pip freeze
black==20.8b1
isort==5.7.0
...
```

And create this file with the following configurations (it goes without saying that you need to edit the package versions accordingly).

`.pre-commit-config.yaml`

```yaml
repos:
  - repo: https://github.com/pycqa/isort
    rev: 5.7.0
    hooks:
      - id: isort
  - repo: https://github.com/psf/black
    rev: 20.8b1
    hooks:
      - id: black
```

If you haven't already initialize your git repository (`git init`), and run.

```shell
pre-commit install
```

Now `pre-commit` will run automatically on `git commit`!

---

## vscode_setup

`.vscode/settings.json`

```json
{
    "python.formatting.provider": "black",
    "python.linting.pylintEnabled": false,
    "python.linting.flake8Enabled": true,
    "python.linting.enabled": true,
    "[python]": {
        "editor.formatOnSave": true,
        "editor.codeActionsOnSave": {
            "source.organizeImports": true
        }
    },
}
```

### helpful_links

- vscode formatters - <https://code.visualstudio.com/docs/python/editing#_formatterspecific-settings>
- vscode linters - <https://code.visualstudio.com/docs/python/linting#_specific-linters>
