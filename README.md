# Awesome Runtime Python

[![Awesome](https://awesome.re/badge.svg)](https://awesome.re)

Inspired by [awesome-python](https://github.com/vinta/awesome-python).

## Contents

- [Awesome Runtime Python](#awesome-runtime-python)
  - [Contents](#contents)
  - [python_setup](#python_setup)
  - [project_setup](#project_setup)
  - [best_practices](#best_practices)

## python_setup

- [pyenv](python_setup/README.md#pyenv) - the preferred way to install python
- [docker](python_setup/README.md#docker) - docker all the way
- [other_options](python_setup/README.md#other_options) - if all else fails you have other options

## project_setup

- [initial_setup](project_setup/README.md#initial_setup) - how to setup your project directory
- [packages](project_setup/README.md#packages) - packages, modules and naming conventions
- [style_guides](project_setup/README.md#style_guides) - PEP 8, linters and formatters

## best_practices

- [the_zen_of_python](best_practices/README.md#the_zen_of_python) - import this
